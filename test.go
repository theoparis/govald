package main

import (
	"fmt"
	"govald/govald"
)

type User struct {
	username string
}

func main() {
	var schema govald.Schema
	schema.Prop("username").Default(func() interface{} { return "admin" })

	var user User
	err := schema.ParseFile("./test.json5", user)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(user)
}
