package govald

import (
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"io/ioutil"
	"os"
	"reflect"
)

type ValueCallback = func() interface{}
type Schema struct {
	properties   map[string]Schema
	defaultValue ValueCallback
}

func (sb Schema) Prop(name string) Schema {
	if sb.properties == nil {
		sb.properties = make(map[string]Schema)
	}
	sb.properties[name] = Schema{}
	return sb.properties[name]
}

func (sb Schema) Default(value ValueCallback) Schema {
	sb.defaultValue = value
	return sb
}

func (sb Schema) Parse(config map[string]interface{}) map[string]interface{} {
	for key, element := range sb.properties {
		val := reflect.ValueOf(config[key])
		if val.Type() == reflect.TypeOf(sb) {
			m := config[key].(map[string]interface{})

			config[key] = element.Parse(m)
		}

		if _, ok := config[key]; !ok || (ok && config[key] == nil) {
			config[key] = element.defaultValue()
		}
	}
	return config
}

func (sb Schema) ParseFile(path string, result interface{}) error {
	jsonFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer func() {
		err = jsonFile.Close()

		if err != nil {
			fmt.Println(err)
		}
	}()

	byteValue, err := ioutil.ReadAll(jsonFile)

	if err != nil {
		fmt.Println(err)
	}

	var config map[string]interface{}

	err = json.Unmarshal(byteValue, &config)
	if err != nil {
		fmt.Println(err)
	}
	parsed := sb.Parse(config)
	fmt.Println(parsed["username"])
	err = mapstructure.Decode(parsed, &result)
	return err
}
